import ImageLoader
import LogisticRegression
import numpy as np
from matplotlib import pyplot as plt

train_set_x_raw, train_set_y = ImageLoader.get_images_matrix("train") 
test_set_x_raw, test_set_y = ImageLoader.get_images_matrix("test")

print("Matrixes shapes loaded from images")
print(train_set_x_raw.shape)
print(train_set_y.shape)
print(test_set_x_raw.shape)
print(test_set_y.shape)

# Capture settings
m_train = train_set_x_raw.shape[0]
m_test = test_set_x_raw.shape[0]
num_px = train_set_x_raw.shape[1]

#Flatten and transpose image matrix
flat_train_x = train_set_x_raw.reshape(train_set_x_raw.shape[0], -1).T
flat_test_x = test_set_x_raw.reshape(test_set_x_raw.shape[0], -1).T
print("flattened train matrix")
print(flat_train_x.shape)
print("flattened test matrix")
print(flat_test_x.shape)

train_set_x = flat_train_x/255.
test_set_x = flat_test_x/255.

d = LogisticRegression.model(train_set_x, train_set_y, test_set_x, test_set_y, num_iterations = 10000, learning_rate = 0.005, print_cost = True)

costs = np.squeeze(d['costs'])
plt.plot(costs)
plt.ylabel('cost')
plt.xlabel('iterations (per hundreds)')
plt.title("Learning rate =" + str(d["learning_rate"]))
plt.show()