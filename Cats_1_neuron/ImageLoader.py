from PIL import Image
from matplotlib import pyplot as plt
import glob
import os, sys
import numpy as np
import matplotlib.image as mpimg
import random

# Constant hardcoded stuff
images_dir = "C:/ML/dogs_vs_cats_learn/"
expected_shape = (64, 64, 3)
def resize(dir_to_resize):
    dirs = os.listdir( dir_to_resize )
    for item in dirs:
        if os.path.isfile(dir_to_resize+item):
            im = Image.open(dir_to_resize+item)
            f, e = os.path.splitext(dir_to_resize+item)
            imResize = im.resize((64,64), Image.ANTIALIAS)
            imResize.save(f + '.jpg', 'JPEG', quality=90)

def get_images_matrix(purpose): #purpose is child dir, either "train" or "test"
    x = []
    y = []
    images_batch = glob.glob(os.path.join(images_dir, purpose, '*.jpg'))
    random.shuffle(images_batch)
    #print(len(images_batch))
    for filename in images_batch:
        #print(os.path.basename(filename))
        single_image = mpimg.imread(filename)
        if single_image.shape != expected_shape:
            print("File: " + os.path.basename(filename)+"----> Shape: " + str(single_image.shape))
        x.append(single_image)
        #print(filename)
        if "cat" in os.path.basename(filename):
            y.append(1)
        elif "dog" in os.path.basename(filename):
            y.append(0)
        elif "COCO" in os.path.basename(filename):
            y.append(0)
        else:
            raise Exception("Neither dog or cat found in filename. Something fishy goin on!!!", filename)
    return np.asarray(x), np.atleast_2d(y) 


x, y = get_images_matrix("train")
print(x.shape)
#print(y)

#resize("C:/ML/dogs_vs_cats_learn/train/")